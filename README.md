 # *willyconf*, a 'willy' configuration file generator.

As a reminder, **willy** is a Linux Markdown text-formatting program producing output suitable for simple fixed-width terminal windows. Refer to [**willy**](https://gitlab.com/mubunt/willy) utility and the [**libZoe**](https://gitlab.com/mubunt/libZoe) library.

## LICENSE
**willyconf** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ willyconf -h
willyconf - Copyright (c) 2020, Michel RIZZO. All Rights Reserved.
willyconf - Version 1.0.0

A 'willy' configuration file generator

Usage: willyconf [OPTIONS]...

  -h, --help         Print help and exit
  -V, --version      Print version and exit
  -o, --output=FILE  Configuration file.

Exit: returns a non-zero status if an error is detected.

$ willyconf -V
willyconf - Copyright (c) 2020, Michel RIZZO. All Rights Reserved.
willyconf - Version 1.0.0

$ willyconf -o willy.cf

```
![Example 1](./README_images/willyconf01.png  "Progress 1")

![Example 1](./README_images/willyconf02.png  "Progress 2")

![Example 1](./README_images/willyconf03.png  "Progress 3")

![Example 1](./README_images/willyconf04.png  "Progress 4")
``` bash
$ cat willy.cf
; Configuration file example for willy.
; ====================================
;
[general]
syntax-highlighting=on
[header1]
foregroundcolor=red
attribut=bold
[header2]
foregroundcolor=yellow
attribut=boldunderline
[header3]
foregroundcolor=green
attribut=underline
[header4]
foregroundcolor=black
attribut=underline
[header5]
foregroundcolor=white
attribut=boldunderline
[header6]
foregroundcolor=white
attribut=italic
[link]
foregroundcolor=red
attribut=bold
[image]
foregroundcolor=blue
attribut=bold
[table]
class=default
[code]
$
```

## STRUCTURE OF THE APPLICATION
This section walks you through **willyconf**'s structure. Once you understand this structure, you will easily find your way around in **willyconf**'s code base.

``` bash
$ yaTree
./                      # Application level
├── README_images/      # Images for documentation
│   ├── willyconf01.png # 
│   ├── willyconf02.png # 
│   ├── willyconf03.png # 
│   └── willyconf04.png # 
├── src/                # Source directory
│   ├── Makefile        # Makefile
│   ├── willyconf.c     # Main program
│   └── willyconf.ggo   # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── COPYING.md          # GNU General Public License markdown file
├── LICENSE.md          # License markdown file
├── Makefile            # Makefile
├── README.md           # ReadMe markdown file
├── RELEASENOTES.md     # Release Notes markdown file
└── VERSION             # Version identification text file

2 directories, 13 files
$ 
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd willyconf
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
```Shell
$ cd willyconf
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
```

## SOFTWARE REQUIREMENTS
- For usage, nothing particular...
- For development:
   - *GengetOpt* binary package installed, version 2.22.6.
   - *[libZoe](https://gitlab.com/mubunt/libZoe)*, a C library for LINUX providing an API for developing simple text-based environments.
- Developped and tested on XUBUNTU 19.04, GCC v8.3.0

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***