//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: willyconf
// A 'willy' configuration file generator
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdbool.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe.h"
#include "willyconf_cmdline.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define WILLY 				"willy"
#define TITLE 				"WILLY: CONFIGURATION FILE GENERATOR"
#define ERRORPREFIX			"WILLY CONFIGURATOR:"

#define FOREGROUND			white

#define SECTIONS			10

#define NO 					1
#define OFF 				1
#define BACKGROUND 			1

#define SECTION_COLUMN1		10
#define SECTION_COLUMN2		40

#define BREAK_IF_ABORT(n)	if (n == ZOE_ABORT) { abort = true; break; }
#define ARRAY_SIZE(x)		(sizeof(x) / sizeof((x)[0]))
#define CLEAR_OBJECT(x)		if (x[isection] != -1) zoe_clearObject(x[isection])
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
struct recordconfig {
	int foregroundcolor;
	int attributes;
	int backgroundcolor;
	int syntaxhighlighting;
	int table;
};
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static struct gengetopt_args_info	args_info;
static const char *section[SECTIONS] = {
	/* 0 */	"HEADER1:",
	/* 1 */	"HEADER2:",
	/* 2 */	"HEADER3:",
	/* 3 */	"HEADER4:",
	/* 4 */	"HEADER5:",
	/* 5 */	"HEADER6:",
	/* 6 */	"LINK:",
	/* 7 */	"IMAGE:",
	/* 8 */	"TABLE:",
	/* 9 */ "CODE:"
};
static const char *gensection[SECTIONS] = {
	/* 0 */	"[header1]",
	/* 1 */	"[header2]",
	/* 2 */	"[header3]",
	/* 3 */	"[header4]",
	/* 4 */	"[header5]",
	/* 5 */	"[header6]",
	/* 6 */	"[link]",
	/* 7 */	"[image]",
	/* 8 */	"[table]",
	/* 9 */ "[code]"
};
static const char *colors[] 		= { "black", "red", "green", "yellow", "blue", "magenta", "cyan", "white" };
static const char *attributes[]		= { "bold", "boldunderline", "underline", "italic", "normal" };
static const char *validation[]		= { "Yes", "No" };
static const char *syntax[]			= { "on", "off" };
static const char *presentation[]	= { "default", "background" };
static const char *table[]			= { "default", "horizon", "zebra", "reverse" };

static struct recordconfig configuration[SECTIONS];

static char tmpfile1[] = { "/tmp/willyConfigXXXXXX" };
static char tmpfile2[] = { "/tmp/willyMDXXXXXX" };
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void fatal( const char *format, ... ) {
	va_list argp;
	char buff[512];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	va_end(argp);
	fprintf(stderr, "%s %s\n\n", ERRORPREFIX, buff);
	exit(EXIT_FAILURE);
}
//------------------------------------------------------------------------------
static void remove_tmpfiles() {
	unlink(tmpfile1);
	unlink(tmpfile2);
}

static void get_tmpfiles() {
	int fd;
	if (-1 == (fd = mkstemp(tmpfile1)))
		fatal("Cannot create temporary file '%s'.", tmpfile1);
	close(fd);
	if (-1 == (fd = mkstemp(tmpfile2)))
		fatal("Cannot create temporary file '%s'.", tmpfile2);
	close(fd);
}
//------------------------------------------------------------------------------
static bool can_run_command(const char *cmd) {
	if(strchr(cmd, '/')) {
		// if cmd includes a slash, no path search must be performed,
		// go straight to checking if it's executable
		return access(cmd, X_OK) == 0;
	}
	const char *path = getenv("PATH");
	if (!path) return false; // something is horribly wrong...
	// we are sure we won't need a buffer any longer
	char *buf = malloc(strlen(path)+strlen(cmd)+3);
	for (; *path; ++path) {
		// start from the beginning of the buffer
		char *p = buf;
		// copy in buf the current path element
		for (; *path && *path!=':'; ++path,++p) *p = *path;
		// empty path entries are treated like "."
		if (p==buf) *p++='.';
		// slash and command name
		if (p[-1]!='/') *p++='/';
		strcpy(p, cmd);
		// check if we can execute it
		if (access(buf, X_OK)==0) {
			free(buf);
			return true;
		}
		// quit at last cycle
		if (!*path) break;
	}
	// not found
	free(buf);
	return false;
}

static void execute( void ) {
	FILE *pf;
	char command[1024], buffer[1024];
	sprintf(command, "%s --config %s %s", WILLY, tmpfile1, tmpfile2);
	if (! (pf = popen(command,"r"))) {
		remove_tmpfiles();
		fatal("%s", "Cannot open temporary output file for coloring code.");
	}
	while (NULL != fgets(buffer, sizeof(buffer), pf)) {
		if (buffer[strlen(buffer) - 1] == '\n') buffer[strlen(buffer) - 1] = '\0';
		fprintf(stdout, "%s\n", buffer);
	}
	pclose(pf);
}
//------------------------------------------------------------------------------
static void show( int isection, int foregroundcolor, int iattributes, int backgroundcolor, int syntaxhighlighting, int itable) {
	FILE *tmp_fd1, *tmp_fd2;
	if (NULL == (tmp_fd1 = fopen(tmpfile1, "w"))) {
		remove_tmpfiles();
		fatal("Cannot open temporary file '%s'.", tmpfile1);
	}
	if (NULL == (tmp_fd2 = fopen(tmpfile2, "w"))) {
		remove_tmpfiles();
		fatal("Cannot open temporary file '%s'.", tmpfile2);
	}
	fprintf(tmp_fd1,"%s\n", gensection[isection]);
	switch (isection) {
	case 0:
		fprintf(tmp_fd1,"foregroundcolor=%s\n", colors[foregroundcolor]);
		fprintf(tmp_fd1,"attribut=%s\n", attributes[iattributes]);
		fprintf(tmp_fd2, "# Header1 *with additional information*\n");
		break;
	case 1:
		fprintf(tmp_fd1,"foregroundcolor=%s\n", colors[foregroundcolor]);
		fprintf(tmp_fd1,"attribut=%s\n", attributes[iattributes]);
		fprintf(tmp_fd2, "## Header2 *with additional information*\n");
		break;
	case 2:
		fprintf(tmp_fd1,"foregroundcolor=%s\n", colors[foregroundcolor]);
		fprintf(tmp_fd1,"attribut=%s\n", attributes[iattributes]);
		fprintf(tmp_fd2, "### Header3 *with additional information*\n");
		break;
	case 3:
		fprintf(tmp_fd1,"foregroundcolor=%s\n", colors[foregroundcolor]);
		fprintf(tmp_fd1,"attribut=%s\n", attributes[iattributes]);
		fprintf(tmp_fd2, "#### Header4 *with additional information*\n");
		break;
	case 4:
		fprintf(tmp_fd1,"foregroundcolor=%s\n", colors[foregroundcolor]);
		fprintf(tmp_fd1,"attribut=%s\n", attributes[iattributes]);
		fprintf(tmp_fd2, "##### Header5 *with additional information*\n");
		break;
	case 5:
		fprintf(tmp_fd1,"foregroundcolor=%s\n", colors[foregroundcolor]);
		fprintf(tmp_fd1,"attribut=%s\n", attributes[iattributes]);
		fprintf(tmp_fd2, "###### Header6 *with additional information*\n");
		break;
	case 6:
		fprintf(tmp_fd1,"foregroundcolor=%s\n", colors[foregroundcolor]);
		fprintf(tmp_fd1,"attribut=%s\n", attributes[iattributes]);
		fprintf(tmp_fd2, "Refer to file [RELEASENOTES](../RELEASENOTES.md).\n");
		fprintf(tmp_fd2, "[Get sources](https://github.com/mubunt/libPager)\n");
		break;
	case 7:
		fprintf(tmp_fd1,"foregroundcolor=%s\n", colors[foregroundcolor]);
		fprintf(tmp_fd1,"attribut=%s\n", attributes[iattributes]);
		fprintf(tmp_fd2, "![Image 1](./1.jpeg  \"Image 1\")\n");
		fprintf(tmp_fd2, "![Image 2](./2.jpeg  \"This is the second image\")\n");
		break;
	case 8:
		if (itable != -1) fprintf(tmp_fd1,"class=%s\n", table[itable]);
		fprintf(tmp_fd2, "| Tables        | Are           | Cool  |\n");
		fprintf(tmp_fd2, "| ------------- |:-------------:| -----:|\n");
		fprintf(tmp_fd2, "| col 3 is      | right-aligned | $1600 |\n");
		fprintf(tmp_fd2, "| col 2 is      | centered      |   $12 |\n");
		fprintf(tmp_fd2, "| zebra stripes | are neat      |    $1 |\n");
		break;
	case 9:
		if (foregroundcolor != -1) fprintf(tmp_fd1,"foregroundcolor=%s\n", colors[foregroundcolor]);
		if (backgroundcolor != -1) fprintf(tmp_fd1,"backgroundcolor=%s\n", colors[backgroundcolor]);
		if (iattributes != -1) fprintf(tmp_fd1,"attribut=%s\n", attributes[iattributes]);
		fprintf(tmp_fd1,"[general]\n");
		fprintf(tmp_fd1,"syntax-highlighting=%s\n", syntax[syntaxhighlighting]);
		fprintf(tmp_fd2, "```C\n");
		fprintf(tmp_fd2, "#include \"pager.h\"\n");
		fprintf(tmp_fd2, "int pager_view( char *filename,  char *header, enum epager_view viewtype);\n");
		fprintf(tmp_fd2, "char datafile[128] = \".libpagerTest.txt\";\n");
		fprintf(tmp_fd2, "char header[128] = \"AUTO-TEST MODE\";\n");
		fprintf(tmp_fd2, "FILE *fp = NULL;\n");
		fprintf(tmp_fd2, "```\n");
		break;
	}
	fclose(tmp_fd1);
	fclose(tmp_fd2);
	execute();
}
//------------------------------------------------------------------------------
static void init_record() {
	for (int i = 0; i < SECTIONS; i++) {
		configuration[i].foregroundcolor = -1;
		configuration[i].attributes = -1;
		configuration[i].backgroundcolor = -1;
		configuration[i].syntaxhighlighting = -1;
		configuration[i].table = -1;
	}
}

static void record( int isection, int foregroundcolor, int attributes, int backgroundcolor, int syntaxhighlighting, int itable) {
	configuration[isection].foregroundcolor = foregroundcolor;
	configuration[isection].attributes = attributes;
	configuration[isection].backgroundcolor = backgroundcolor;
	configuration[isection].syntaxhighlighting = syntaxhighlighting;
	configuration[isection].table = itable;
}
//------------------------------------------------------------------------------
static void generation( char *file ) {
	FILE *fd;
	fprintf(stdout, "Generation of WILLY configuration file '%s'.....", file);
	if (NULL == (fd = fopen(file, "w"))) {
		remove_tmpfiles();
		fatal("Cannot create output file '%s'.", file);
	}
	fprintf(fd, "; Configuration file example for willy.\n");
	fprintf(fd, "; ====================================\n");
	fprintf(fd, ";\n");
	fprintf(fd, "[general]\n");
	if (configuration[8].syntaxhighlighting == 1)
		fprintf(fd, "syntax-highlighting=off\n");
	else
		fprintf(fd, "syntax-highlighting=on\n");

	for (int i = 0; i < SECTIONS; i++) {
		fprintf(fd,"%s\n", gensection[i]);
		if (i == 8) {
			if (configuration[i].table != -1) fprintf(fd,"class=%s\n", table[configuration[i].table]);
		} else {
			if (i == 9) {
				if (configuration[i].backgroundcolor != -1) fprintf(fd,"backgroundcolor=%s\n", colors[configuration[i].backgroundcolor]);
			}
			if (configuration[i].foregroundcolor != -1) fprintf(fd,"foregroundcolor=%s\n", colors[configuration[i].foregroundcolor]);
			if (configuration[i].attributes != -1) fprintf(fd,"attribut=%s\n", attributes[configuration[i].attributes]);
		}
	}
	fclose(fd);
}
//------------------------------------------------------------------------------
// MAIN FUNCTION
//------------------------------------------------------------------------------
int main( int argc, char **argv ) {
	int lines[SECTIONS + 1];
	objectID id1[SECTIONS], id2[SECTIONS], id3[SECTIONS];
	objectID id4[SECTIONS], id5[SECTIONS], id6[SECTIONS];
	objectID id;
	int report;
	int iforegroundcolor = -1;
	int iattributes = -1;
	int ibackgroundcolor = -1;
	int ibackgroundform = -1;
	int itable = -1;
	int isyntax = -1;
	int ivalid = -1;
	bool stop = false, abort = false;
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_willyconf(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	//---- Chech if willy is runnable ------------------------------------------
	if (! can_run_command(WILLY))
		fatal("'%s' utility is not runnable.", WILLY);
	//----  Initialization -----------------------------------------------------
	get_tmpfiles();
	init_record();
	memset(id1, -1, sizeof(id1));
	memset(id2, -1, sizeof(id2));
	memset(id3, -1, sizeof(id3));
	memset(id4, -1, sizeof(id4));
	memset(id5, -1, sizeof(id5));
	memset(id6, -1, sizeof(id6));
	//----  Go on --------------------------------------------------------------
	zoe_init(15, 85, WITH_LOG);
	zoe_setattributes(ZOE_OBJECT_QUESTION | ZOE_OBJECT_MENU, FOREGROUND, boldoff);
	zoe_setattributes(ZOE_OBJECT_TITLE | ZOE_OBJECT_TEXT | ZOE_OBJECT_ANSWER, FOREGROUND, boldon);

	int stage = 0;
	int isection = 0;
	int previous_stage = 0;

	zoe_title(TITLE);
	lines[isection] = zoe_getnextfreeline();

#define TERMINAL_STAGE	41

	while (1) {
		switch (stage) {
		case 0:
		case 4:
		case 8:
		case 12:
		case 16:
		case 20:
		case 24:
		case 28:
		case 32:
		case 35:
			previous_stage = stage;
			CLEAR_OBJECT(id1);
			zoe_text(&id1[isection], lines[isection], 1, section[isection]);
			lines[isection + 1] = zoe_getnextfreeline();
			++stage;
			break;
		case 1:
		case 5:
		case 9:
		case 13:
		case 17:
		case 21:
		case 25:
		case 29:
			previous_stage = stage;
			ibackgroundcolor = isyntax = itable = -1;
			CLEAR_OBJECT(id2);
			iforegroundcolor = zoe_close_question(&id2[isection], lines[isection], SECTION_COLUMN1, "Foreground color?", colors, ARRAY_SIZE(colors));
			BREAK_IF_ABORT(iforegroundcolor);
			if (iforegroundcolor == ZOE_BACK) {
				zoe_clearObject(id1[isection]);
				if (--isection < 0) abort = true;
				else stage -= 3;
			} else
				++stage;
			break;
		case 2:
		case 6:
		case 10:
		case 14:
		case 18:
		case 22:
		case 26:
		case 30:
			previous_stage = stage;
			CLEAR_OBJECT(id3);
			iattributes = zoe_close_question(&id3[isection], lines[isection], SECTION_COLUMN2, "Attribut?", attributes, ARRAY_SIZE(attributes));
			BREAK_IF_ABORT(iattributes);
			if (iattributes == ZOE_BACK) --stage;
			else ++stage;
			break;
		case 3:
		case 7:
		case 11:
		case 15:
		case 19:
		case 23:
		case 27:
		case 31:
		case 34:
		case TERMINAL_STAGE:
			show(isection, iforegroundcolor, iattributes, ibackgroundcolor, isyntax, itable);
			int n = lines[isection] + 1;
			if (previous_stage == 39 || previous_stage == 40) n = lines[isection] + 3;
			ivalid = zoe_close_question(&id, n, SECTION_COLUMN1, "Does this please you?", validation, ARRAY_SIZE(validation));
			zoe_clearObject(id);
			BREAK_IF_ABORT(ivalid);
			if (ivalid == ZOE_BACK || ivalid == NO)
				if (previous_stage == 40) stage -= 2;
				else stage = previous_stage;
			else {
				record(isection, iforegroundcolor, iattributes, ibackgroundcolor, isyntax, itable);
				if (isection == 9)
					stop = true;
				else {
					++isection;
					++stage;
				}
			}
			break;
		case 33:
			previous_stage = stage;
			iforegroundcolor = iattributes = -1;
			CLEAR_OBJECT(id2);
			itable =  zoe_close_question(&id2[isection], lines[isection], SECTION_COLUMN1, "Presentation?", table, ARRAY_SIZE(table));
			BREAK_IF_ABORT(itable);
			if (itable == ZOE_BACK) {
				zoe_clearObject(id1[isection]);
				--isection;
				stage -= 3;
			} else
				++stage;
			break;
		case 36:
			previous_stage = stage;
			itable = -1;
			CLEAR_OBJECT(id2);
			isyntax =  zoe_close_question(&id2[isection], lines[isection], SECTION_COLUMN1, "Syntax Highlighting?", syntax, ARRAY_SIZE(syntax));
			BREAK_IF_ABORT(isyntax);
			if (isyntax == ZOE_BACK) {
				zoe_clearObject(id1[isection]);
				--isection;
				stage -= 3;
			} else {
				if (isyntax == OFF)
					++stage;
				else
					stage = TERMINAL_STAGE;
			}
			break;
		case 37:
			previous_stage = stage;
			CLEAR_OBJECT(id3);
			iforegroundcolor = zoe_close_question(&id3[isection], lines[isection] + 1, SECTION_COLUMN1, "Foreground color?", colors, ARRAY_SIZE(colors));
			BREAK_IF_ABORT(iforegroundcolor);
			if (iforegroundcolor == ZOE_BACK) {
				zoe_clearObject(id2[isection]);
				--stage;
			} else
				++stage;
			break;
		case 38:
			previous_stage = stage;
			CLEAR_OBJECT(id4);
			iattributes = zoe_close_question(&id4[isection], lines[isection] + 1, SECTION_COLUMN2, "Attribut?", attributes, ARRAY_SIZE(attributes));
			BREAK_IF_ABORT(iattributes);
			if (iattributes == ZOE_BACK) {
				zoe_clearObject(id3[isection]);
				--stage;
			} else
				++stage;
			break;
		case 39:
			previous_stage = stage;
			CLEAR_OBJECT(id5);
			ibackgroundform = zoe_close_question(&id5[isection], lines[isection] + 2, SECTION_COLUMN1, "Presentation?", presentation, ARRAY_SIZE(presentation));
			BREAK_IF_ABORT(ibackgroundform);
			if (ibackgroundform == ZOE_BACK) {
				zoe_clearObject(id4[isection]);
				--stage;
			} else if (ibackgroundform == BACKGROUND) ++stage;
			else stage = TERMINAL_STAGE;
			break;
		case 40:
			previous_stage = stage;
			CLEAR_OBJECT(id6);
			ibackgroundcolor =  zoe_close_question(&id6[isection], lines[isection] + 2, SECTION_COLUMN2, "Background color?", colors, ARRAY_SIZE(colors));
			BREAK_IF_ABORT(ibackgroundcolor);
			if (ibackgroundcolor == ZOE_BACK) {
				zoe_clearObject(id5[isection]);
				--stage;
			} else
				stage = TERMINAL_STAGE;
			break;
		}
		if (stop || abort) break;
	}
	if (abort)
		report = EXIT_FAILURE;
	else {
		if (ZOE_GO == zoe_gonogo(&id, "Go on with the file generation?", "Yes", "No"))
			generation(args_info.output_arg);
		report = EXIT_SUCCESS;
	}
	zoe_end();
	remove_tmpfiles();
	cmdline_parser_willyconf_free(&args_info);
	return report;
}
//==============================================================================
